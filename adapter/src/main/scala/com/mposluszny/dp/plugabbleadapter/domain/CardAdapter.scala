package com.mposluszny.dp.plugabbleadapter.domain

/**
 * This plugabble adapter accepts any memory card as the adaptee
 * and is able to read its contents
 */
class CardAdapter(private var _memoryCard: MemoryCard) {
  def readStoredData() = _memoryCard.readData()
  def memoryCard_(x: MemoryCard) = { _memoryCard = x }
  
  // ----- //
//  def insertMicroSDCard(x: MicroSDCard) = { _memoryCard = x }
//  def insertSDCard(x: SDCard) = { _memoryCard = x }
//  def readMicroSDCardData(x: MicroSDCard) = x.readData()
//  def readSDCardData(x: SDCard) = x.readData()
  
  // ----- //  
//  private var _storedData: String = null
//  def insertMicroSDCard(x: MicroSDCard) = { _storedData = x.readData() }
//  def insertSDCard(x: SDCard) = { _storedData = x.readData() }
//  def storedData = _storedData
}